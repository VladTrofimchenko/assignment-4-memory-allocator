#include "test.h"
#include "mem.h"
#include "mem_internals.h"
#define TEST_1_HEAP_SIZE 77777
#define TESTS_HEAP_SIZE 10000

static void destroy_heap(void* heap, int count_bytes){
    munmap(heap, size_from_capacity((block_capacity){.bytes = count_bytes}).bytes);
}

static struct block_header* block_get_header(void* contents){
    return (struct block_header*)((uint8_t*)contents - offsetof(struct block_header, contents));
}




void test1(void){
    printf("TEST1\nDefault malloc\n");
    void* heap = heap_init(TEST_1_HEAP_SIZE);
    if(!heap){
        fprintf(stderr, "ERR: Heap wasn't init\n");
        return;
    }
    printf("Heap after init:\n");
    debug_heap(stdout, heap);
    void* mem_alloc = _malloc(500);
    if(!mem_alloc){
        fprintf(stderr, "ERR: Can't malloc\n");
        destroy_heap(heap, TEST_1_HEAP_SIZE);
        return;
    }
    printf("Heap after malloc:\n");
    debug_heap(stdout, heap);
    _free(mem_alloc);
    printf("Free memory\n");
    debug_heap(stdout, heap);
    destroy_heap(heap, TEST_1_HEAP_SIZE);
    printf("TEST1 PASSED!\n");
    printf("\n\n");
}

void test2(void){
    printf("TEST2\nFree block after allocated\n");

    void* heap = heap_init(TESTS_HEAP_SIZE);
    if(!heap){
        fprintf(stderr, "ERR: Heap wasn't init\n");
        return;
    }
    printf("Heap after init:\n");
    debug_heap(stdout, heap);

    void* mem_alloc1 = _malloc(600);
    printf("Heap after 1st malloc:\n");
    debug_heap(stdout, heap);

    void* mem_alloc2 = _malloc(700);
    printf("Heap after 2nd malloc:\n");
    debug_heap(stdout, heap);

    if(!mem_alloc1 || !mem_alloc2){
        fprintf(stderr, "ERR: Can't malloc");
        if (mem_alloc1){
            _free(mem_alloc1);
        }
        else if (mem_alloc2){
            _free(mem_alloc2);
        }
        destroy_heap(heap, TESTS_HEAP_SIZE);
        return;
    }

    _free(mem_alloc1);
    printf("Free 1st malloc\n");
    debug_heap(stdout, heap);


    destroy_heap(heap, TESTS_HEAP_SIZE);
    printf("TEST2 PASSED!\n");
    printf("\n\n");

}

void test3(void){
    printf("TEST3\nFreeing 2 blocks\n");

    void* heap = heap_init(TESTS_HEAP_SIZE);
    if(!heap){
        fprintf(stderr, "ERR: Heap wasn't init\n");
        return;
    }
    printf("Heap after init:\n");
    debug_heap(stdout, heap);

    void* mem_alloc1 = _malloc(111);
    printf("Heap after 1st malloc:\n");
    debug_heap(stdout, heap);

    void* mem_alloc2 = _malloc(222);
    printf("Heap after 2nd malloc:\n");
    debug_heap(stdout, heap);

    void* mem_alloc3 = _malloc(333);
    printf("Heap after 2nd malloc:\n");
    debug_heap(stdout, heap);


    if(!mem_alloc1 || !mem_alloc2 || !mem_alloc3){
        fprintf(stderr, "ERR: Can't malloc");
        if (mem_alloc1){
            _free(mem_alloc1);
        }
        else if (mem_alloc2){
            _free(mem_alloc2);
        }
        else if (mem_alloc3){
            _free(mem_alloc3);
        }
        else if (mem_alloc1 && mem_alloc2){
            _free(mem_alloc1);
            _free(mem_alloc2);
        }
        else if (mem_alloc1 && mem_alloc3){
            _free(mem_alloc1);
            _free(mem_alloc3);
        }
        else if (mem_alloc2 && mem_alloc3){
            _free(mem_alloc2);
            _free(mem_alloc3);
        }
        destroy_heap(heap, TESTS_HEAP_SIZE);
        return;
    }

    _free(mem_alloc1);
    printf("Free 1st malloc\n");
    debug_heap(stdout, heap);

    _free(mem_alloc3);
    printf("Free 3rd malloc\n");
    debug_heap(stdout, heap);


    destroy_heap(heap, TESTS_HEAP_SIZE);
    printf("TEST3 PASSED!\n");
    printf("\n\n");
}

void test4(void){
    printf("TEST4\nRegion is over, growing up new region\n");

    void* heap = heap_init(TESTS_HEAP_SIZE);
    if(!heap){
        fprintf(stderr, "ERR: Heap wasn't init\n");
        return;
    }
    printf("Heap after init:\n");
    debug_heap(stdout, heap);

    void* mem_alloc1 = _malloc(5000);
    void* mem_alloc2 = _malloc(9000);
    
    if(!mem_alloc1 || !mem_alloc2){
        fprintf(stderr, "ERR: Can't malloc\n");
        if (mem_alloc1){
            _free(mem_alloc1);
        }
        else if (mem_alloc2){
            _free(mem_alloc2);
        }
        destroy_heap(heap, TESTS_HEAP_SIZE);
        return;
    }

    printf("Heap after 2 mallocs:\n");
    debug_heap(stdout, heap);

    struct block_header* header_block1 = block_get_header(mem_alloc1);
    struct block_header* header_block2 = block_get_header(mem_alloc2);

    if(header_block1->next != header_block2){
        fprintf(stderr, "ERR: Wrong growing\n");
        return;
    }




    _free(mem_alloc1);
    printf("Free 1st malloc\n");
    debug_heap(stdout, heap);

    _free(mem_alloc2);
    printf("Free 2nd malloc\n");
    debug_heap(stdout, heap);


    destroy_heap(heap, TESTS_HEAP_SIZE);
    printf("TEST4 PASSED!\n");
    printf("\n\n");

}

void test5(void){
    printf("TEST5\nRegion is over, growing up new region in another place, 'couse place after overed region isn't free\n");

    void* heap = heap_init(TESTS_HEAP_SIZE);
    if(!heap){
        fprintf(stderr, "ERR: Heap wasn't init\n");
        return;
    }
    printf("Heap after init:\n");
    debug_heap(stdout, heap);

    void* mem_alloc1 = _malloc(8000);
    
    if(!mem_alloc1){
        fprintf(stderr, "ERR: Can't malloc1");
        return;
    }

    printf("Heap after 1st malloc:\n");
    debug_heap(stdout, heap);

    

    struct block_header* header_block1 = block_get_header(mem_alloc1);

    (void)mmap(header_block1->contents + header_block1->capacity.bytes, REGION_MIN_SIZE,
            PROT_READ | PROT_WRITE , MAP_PRIVATE | MAP_FIXED , -1 , 0);

    void* mem_alloc2 = _malloc(15000);

    if(!mem_alloc2){
        fprintf(stderr, "ERR: Can't malloc2");
        return;
    }

    printf("Heap after 2nd malloc:\n");
    debug_heap(stdout, heap);
    
    munmap(header_block1->contents + header_block1->capacity.bytes, REGION_MIN_SIZE);

    _free(mem_alloc1);
    _free(mem_alloc2);
    printf("Free 2 mallocs\n");
    debug_heap(stdout, heap);

    destroy_heap(heap, TESTS_HEAP_SIZE);
    printf("TEST5 PASSED!\n");

}

void runTests(){
    test1();
    test2();
    test3();
    test4();
    test5();
}